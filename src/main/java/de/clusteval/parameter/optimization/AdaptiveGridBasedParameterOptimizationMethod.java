/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package de.clusteval.parameter.optimization;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import de.clusteval.data.IDataConfig;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.framework.repository.IRepository;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.parameter.NoParameterSetFoundException;
import de.clusteval.program.DoubleParameter;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.IntegerParameter;
import de.clusteval.program.ParameterSet;
import de.clusteval.program.StringParameter;
import de.clusteval.quality.IQualityMeasure;
import de.clusteval.quality.IQualitySet;
import de.clusteval.run.IParameterOptimizationRun;
import de.clusteval.run.runresult.RunResultParseException;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.ClustEvalAlias;
import de.clusteval.utils.DynamicComponentVersion;
import de.clusteval.utils.IParameter;
import de.clusteval.utils.InternalAttributeException;
import de.clusteval.utils.Parameter;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 * 
 */
@DynamicComponentVersion(version = "8-RC1-SNAPSHOT")
@ClustEvalAlias(alias = "Adaptive Grid-Based Parameter Optimization")
@ClassVersionRequirement(target = ParameterOptimizationMethod.class, versionSpecification = "[3-RC1-SNAPSHOT, 3.1)")
@LoadableClassParentAnnotation(parent = "GridBasedParameterOptimizationMethod:5-RC1-SNAPSHOT")
public class AdaptiveGridBasedParameterOptimizationMethod
		extends
			ParameterOptimizationMethod {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6845561861475307372L;

	protected int remainingIterationCount;

	/**
	 * The number of layers.
	 */
	protected int layerCount;

	protected int iterationsPerLayer;

	protected int currentLayer;

	protected GridBasedParameterOptimizationMethod currentDivisiveMethod;

	protected List<IParameter<?>> originalParameters;

	// private int totalIterationCount;
	protected Map<String, Pair<?, ?>> paramToValueRange;

	/**
	 * @param repo
	 * @param changeDate
	 * @param absPath
	 * @param run
	 * @param programConfig
	 * @param dataConfig
	 * @param params
	 * @param totalIterations
	 * @param isResume
	 * @throws RegisterException
	 */
	public AdaptiveGridBasedParameterOptimizationMethod(final IRepository repo,
			final long changeDate, final File absPath,
			final IParameterOptimizationRun run,
			final IProgramConfig programConfig, final IDataConfig dataConfig,
			List<IParameter<?>> params, int totalIterations,
			final boolean isResume) {
		super(repo, changeDate, absPath, run, programConfig, dataConfig, params,
				totalIterations, isResume);
		this.originalParameters = params;
		// this.totalIterationCount = (int) ArraysExt
		// .product(this.iterationPerParameter);
		this.layerCount = (int) Math.sqrt(this.totalIterationCount);
		// this.iterationsPerLayer = this.totalIterationCount / this.layerCount;
		// this.layerCount = (int) Math
		// .round(Math.log10(this.totalIterationCount));
		this.iterationsPerLayer = this.layerCount > 0
				? this.totalIterationCount / this.layerCount
				: 0;
		this.paramToValueRange = new HashMap<String, Pair<?, ?>>();
	}

	/**
	 * The copy constructor for this method.
	 * 
	 * <p>
	 * Cloning of this method does not keep potentially already initialized
	 * parameter value ranges
	 * 
	 * @param other
	 *            The object to clone.
	 */
	public AdaptiveGridBasedParameterOptimizationMethod(
			final AdaptiveGridBasedParameterOptimizationMethod other) {
		super(other);

		this.originalParameters = Parameter.cloneParameterList(other.params);
		// this.totalIterationCount = (int) ArraysExt
		// .product(this.iterationPerParameter);
		// this.layerCount = (int) Math.sqrt(this.iterationPerParameter[0]);
		// this.iterationsPerLayer = this.totalIterationCount / this.layerCount;
		this.layerCount = other.layerCount;
		this.iterationsPerLayer = other.iterationsPerLayer;
		this.paramToValueRange = new HashMap<String, Pair<?, ?>>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cluster.paramOptimization.ParameterOptimizationMethod#getNextParameterSet
	 * ()
	 */
	@Override
	protected synchronized Pair<Long, ParameterSet> getNextParameterSet(
			final ParameterSet forcedParameterSet)
			throws InternalAttributeException, RegisterException,
			NoParameterSetFoundException, InterruptedException,
			ParameterSetAlreadyEvaluatedException {
		if (this.currentDivisiveMethod == null
				|| (!this.currentDivisiveMethod.hasNext()
						&& this.currentLayer < this.layerCount)) {

			if (forcedParameterSet == null) {
				boolean allParamSetsFinished = false;
				while (!Thread.interrupted() && !allParamSetsFinished) {
					allParamSetsFinished = true;
					if (this.getResult().getIterationsWithoutQuality()
							.size() > 0) {
						allParamSetsFinished = false;
						// this.log.warn("null parameter set: " + set);
						this.wait();
					}
				}
			}
			this.applyNextDivisiveMethod();
		}
		try {
			Pair<Long,
					ParameterSet> result = this.currentDivisiveMethod.next(
							forcedParameterSet,
							this.currentDivisiveMethod.getStartedCount() + 1);

			if (this.getResult().getParameterSets().values()
					.contains(result.getSecond()))
				this.currentDivisiveMethod.giveQualityFeedback(
						result.getFirst(),
						this.getResult().get(result.getFirst()));
			return result;
		} catch (ParameterSetAlreadyEvaluatedException e) {
			// 09.05.2014: we have to adapt the iteration number of the current
			// divisive method to the iteration number of this layered method
			throw new ParameterSetAlreadyEvaluatedException(++this.currentCount,
					this.getResult().getIterationNumberForParameterSet(
							e.getParameterSet()),
					e.getParameterSet());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cluster.paramOptimization.ParameterOptimizationMethod#initParameterValues
	 * ()
	 */
	@Override
	public void initParameterValues()
			throws ParameterOptimizationException, InternalAttributeException {
		super.initParameterValues();

		for (IParameter<?> param : params)
			this.paramToValueRange.put(param.getName(),
					Pair.getPair(
							param.evaluateMinValue(dataConfig.getRepository(),
									dataConfig, programConfig),
							param.evaluateMaxValue(dataConfig.getRepository(),
									dataConfig, programConfig)));
	}

	/**
	 * @throws InternalAttributeException
	 * @throws RegisterException
	 * @throws InterruptedException
	 * 
	 */
	protected void applyNextDivisiveMethod() throws InternalAttributeException,
			RegisterException, InterruptedException {
		/*
		 * First we take the new optimum of the last divisive layer, if there
		 * was one
		 */
		if (this.currentDivisiveMethod != null) {
			for (IQualityMeasure measure : this.currentDivisiveMethod
					.getResult().getOptimalCriterionValue().keySet()) {
				if (measure.isBetterThan(
						this.currentDivisiveMethod.getResult()
								.getOptimalCriterionValue().get(measure),
						this.getResult().getOptimalCriterionValue()
								.get(measure))) {
					this.getResult().getOptimalCriterionValue().put(measure,
							this.currentDivisiveMethod.getResult()
									.getOptimalCriterionValue().get(measure));;
				}
			}
		}
		/*
		 * We adapt the ranges of the parameters to control which points the
		 * divisive method evaluates
		 */
		List<IParameter<?>> newParams = new ArrayList<IParameter<?>>();
		for (IParameter<?> p : params) {
			IParameter<?> param = p.clone();
			// if this is the first layer or the parameter is a string parameter
			// with options, we do not change the value range
			if (this.currentDivisiveMethod != null
					&& !(param instanceof StringParameter
							&& param.isOptionsSet())) {
				/*
				 * In the next layer we half the domains of every parameter
				 * centered around that point with maximal quality
				 */
				double paramOptValue = Double.valueOf(this.getResult()
						.getOptimalParameterSet().get(param.getName()));

				double oldMinValue;
				double oldMaxValue;

				try {
					if (param instanceof DoubleParameter)
						oldMinValue = (Double) (paramToValueRange
								.get(param.getName()).getFirst());
					else
						oldMinValue = (Integer) (paramToValueRange
								.get(param.getName()).getFirst());
					if (param instanceof DoubleParameter)
						oldMaxValue = (Double) (paramToValueRange
								.get(param.getName()).getSecond());
					else
						oldMaxValue = (Integer) (paramToValueRange
								.get(param.getName()).getSecond());
				} catch (ClassCastException e) {
					System.out.println(param);
					System.out.println(paramToValueRange.get(param.getName()));
					System.out.println(paramToValueRange.get(param.getName())
							.getFirst().getClass() + " "
							+ paramToValueRange.get(param.getName()).getSecond()
									.getClass());
					System.out.println(
							paramToValueRange.get(param.getName()).getFirst());
					throw e;
				}

				double oldRange = oldMaxValue - oldMinValue;

				double newMinValue = paramOptValue - oldRange / 4;
				double newMaxValue = paramOptValue + oldRange / 4;

				double origParamMinValue;
				if (param instanceof DoubleParameter)
					origParamMinValue = ((DoubleParameter) param)
							.evaluateMinValue(dataConfig.getRepository(),
									dataConfig, programConfig);
				else
					origParamMinValue = ((IntegerParameter) param)
							.evaluateMinValue(dataConfig.getRepository(),
									dataConfig, programConfig);
				double origParamMaxValue;
				if (param instanceof DoubleParameter)
					origParamMaxValue = ((DoubleParameter) param)
							.evaluateMaxValue(dataConfig.getRepository(),
									dataConfig, programConfig);
				else
					origParamMaxValue = ((IntegerParameter) param)
							.evaluateMaxValue(dataConfig.getRepository(),
									dataConfig, programConfig);

				/*
				 * If we are outside the old minvalue - maxvalue range, we shift
				 * the new range.
				 */
				if (newMinValue < origParamMinValue) {
					// newMaxValue += (origParamMinValue - newMinValue);
					newMinValue += (origParamMinValue - newMinValue);
				} else if (newMaxValue > origParamMaxValue) {
					// newMinValue -= (newMaxValue - origParamMaxValue);
					newMaxValue -= (newMaxValue - origParamMaxValue);
				}

				if (param.getClass().equals(DoubleParameter.class)) {
					paramToValueRange.put(param.getName(),
							Pair.getPair(newMinValue, newMaxValue));

					param = param.withDifferentMin(newMinValue + "");
					param = param.withDifferentMax(newMaxValue + "");
					param = param.withDifferentDefault(newMinValue + "");
				} else if (param.getClass().equals(IntegerParameter.class)) {
					paramToValueRange.put(param.getName(),
							Pair.getPair((int) newMinValue, (int) newMaxValue));

					param = param.withDifferentMin(newMinValue + "");
					param = param.withDifferentMax(newMaxValue + "");
					param = param.withDifferentDefault(newMinValue + "");
				}
			}
			/*
			 * If this is the first layer, we just operate on the whole ranges
			 * of the parameters ( do not change them here)
			 */
			newParams.add(param);
		}

		int newIterationsPerParameter = getNextIterationsPerLayer();
		try {
			this.currentDivisiveMethod = createDivisiveMethod(newParams,
					newIterationsPerParameter);
			this.currentDivisiveMethod
					.reset(new File(this.getResult().getAbsolutePath()));
		} catch (ParameterOptimizationException e) {
			e.printStackTrace();
		} catch (RunResultParseException e) {
			e.printStackTrace();
		}
		this.currentLayer++;
	}

	protected int getNextIterationsPerLayer() {
		int newLayerIterations;

		double remainingIterationCount = this.iterationsPerLayer;
		int remainingParams = this.params.size();
		final List<Integer> iterations = new ArrayList<Integer>();

		// parameters that have a fixed number of options
		for (int i = 0; i < params.size(); i++) {
			final IParameter<?> param = this.params.get(i);
			if (param.getOptions() != null && param.getOptions().length > 0) {
				iterations.add(param.getOptions().length);
				remainingIterationCount /= param.getOptions().length;
				remainingParams--;
			}
		}

		// the iterations for the remaining parameters
		newLayerIterations = (int) Math.pow(Math.floor(
				Math.pow(remainingIterationCount, 1.0 / remainingParams)),
				remainingParams);
		for (Integer i : iterations)
			newLayerIterations *= i;

		if (currentLayer < layerCount - 1) {
			this.remainingIterationCount -= newLayerIterations;
		} else {
			/*
			 * If this is the last layer, do the remaining number of iterations
			 */
			this.remainingIterationCount = 0;
		}
		return newLayerIterations;
	}

	protected GridBasedParameterOptimizationMethod createDivisiveMethod(
			List<IParameter<?>> newParams, int newIterationsPerParameter)
			throws ParameterOptimizationException, RegisterException {
		return new GridBasedParameterOptimizationMethod(repository,
				System.currentTimeMillis(),
				new File("GridBasedParameterOptimizationMethod"), run,
				programConfig, dataConfig, newParams, newIterationsPerParameter,
				false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cluster.paramOptimization.ParameterOptimizationMethod#hasNext()
	 */
	@Override
	public boolean hasNext() {
		boolean layerHasNext = (this.currentDivisiveMethod != null
				? this.currentDivisiveMethod.hasNext()
				: false);
		if (!layerHasNext) {
			return this.currentLayer < this.layerCount;
		}
		return layerHasNext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cluster.paramOptimization.ParameterOptimizationMethod#giveQualityFeedback
	 * (java.util.Map)
	 */
	@Override
	public synchronized void giveQualityFeedback(final long iteration,
			IQualitySet qualities) {
		try {
			super.giveQualityFeedback(iteration, qualities);
			this.currentDivisiveMethod.giveQualityFeedback(iteration,
					qualities);
			// wake up all threads, which are waiting for the parameter sets of
			// the
			// last divisive method to finish.
		} finally {
			this.notifyAll();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cluster.paramOptimization.ParameterOptimizationMethod#reset()
	 */
	@Override
	public void reset(final File absResultPath)
			throws ParameterOptimizationException, InternalAttributeException,
			RegisterException, RunResultParseException, InterruptedException {
		this.currentLayer = 0;
		this.remainingIterationCount = getTotalIterationCount();
		if (this.originalParameters != null)
			this.params = this.originalParameters;
		this.currentDivisiveMethod = null;
		super.reset(absResultPath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cluster.paramOptimization.ParameterOptimizationMethod#
	 * getTotalIterationCount ()
	 */
	@Override
	public int getTotalIterationCount() {
		return this.totalIterationCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cluster.paramOptimization.ParameterOptimizationMethod#
	 * getCompatibleDataSetFormatBaseClasses()
	 */
	@Override
	public List<Class<
			? extends IDataSetFormat>> getCompatibleDataSetFormatBaseClasses() {
		return new ArrayList<Class<? extends IDataSetFormat>>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cluster.paramOptimization.ParameterOptimizationMethod#
	 * getCompatibleProgramClasses()
	 */
	@Override
	public List<String> getCompatibleProgramNames() {
		return new ArrayList<String>();
	}
}
